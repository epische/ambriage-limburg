export function getColorFromType(location) {
  switch (location) {
    case "Podium":
      return "#555555";
    case "Schuim":
      return "#009640";
    case "Chiro Café":
      return "#36A9E1";
    case "Animatie":
      return "#009640";
    case "Food Wood":
      return "#E1ED00";
    case "Markt":
      return "#F39200";
    default:
      return "#F39200";
  }
}
