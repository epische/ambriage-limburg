export const workshops = [
  {
    title: "Buitenlands koken met Ben",
    description:
      "Hongertje? Lekker buitenlands koken samen met Ben!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Jojo met Sander",
    description:
      "Indruk maken op de meisjes/jongens? Sander leert je de hipste trucs met een jojo!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Pimp je Chirorok/broek met Caroline",
    description:
      "Is je Chirorok nog wat leeg? Of heb je pas een nieuwe chiroshort gekocht? Caroline helpt je om er een prachtig plaatje van te maken!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Kampdansen met Kazou ",
    description:
      "Dansez, dansez! Spijker je eigen dansskills bij, leer hoe je een kampdans van a tot z maakt én hoe je hem aan je leden aanleert",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Frisbee met FC Helchteren",
    description:
      "Dé strandsport die terug in de lift zit! De frisbeeclub van Helchteren leert je hoe het moet!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Mocktails met Commissie jeugdbeleid",
    description:
      "Al een opwarmertje voor de fuif? Lekkere mocktails die je daarna misschien wel kan verkopen op de jullie chirofuif!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Fuiven met de Commissie Jeugdbeleid",
    description:
      "Fuiven? Hoe doe je dat nu écht? Met een tikkeltje wetgeving",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Buitenlands Bivak met Wegwijzer 2u!",
    description:
      "Ontdek de geheimen van een buitenlands bivak en wie weet gaan jullie volgend jaar zelf wel naar het buitenland.",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Improvisatie met Villa Basta 2u!",
    description:
      "Spring het podium op zonder tekst, zonder scenario en speel toch de hele zaal plat. Ontdek stap voor stap hoe je improviseert.",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Action Painting met Villa Basta 2u!",
    description:
      "Durf jezelf te smijten en leef je eens helemaal uit! Met allerlei kleuren en soorten verf, kwasten, rollers en spuitbussen creëer je een spetterend kunstwerk",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Graffiti met Villa Basta 2u!",
    description:
      "Improviseer en experimenteer er op los met spuitbussen.",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Dj’en met Villa Basta 2u!",
    description:
      "Hé DJ, draai nog maar een plaat! Leer de geheimen van het scratchen, het beatmixen en draaien kennen en binnen de kortste keren draai jij je eigen platen!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Handlettering met Jane",
    description:
      "Creatief, creatiever, creatiefst! Jane leert je in een mum van tijd de mooiste letters en cijfers schrijven! ",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Totebags Pimpen met Lotte en Michelle",
    description:
      "Niks hipper dan een totebag tegenwoordig! Pimp je totebag en maak er een echt pareltje van!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Reis rond de wereld met Dienst Internationale activiteiten",
    description:
      "DIA neemt je mee op een reis rond de wereld. Leer a.d.h.v. spelletjes jeugdbewegingen van over heel de wereld kennen",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Spelen met diversiteit met Commissie Diversiteit",
    description:
      "Diversiteit zit in alles en is overal. Wil je leren hoe je spelletjes speelt rekening houdend met een diverse groep? Dat kan hier!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Circustechnieken met Niels",
    description:
      "Hang je graag de clown uit? Leef je helemaal uit met balletjes, diabolo’s, Chinese bordjes, devilsticks en zoveel meer!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Capoeira met Karamelo",
    description:
      "Haal je beste moves maar boven! Karamelo leert je Capoeira!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Slijm maken met Kaat ",
    description:
      "Even terug naar je kindertijd? Maak je eigen slijm!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Koken op vuur met Lise",
    description:
      "Koken op een groot vuur? Hoe sjik is da?!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Scheer Schoor Schuim met D'ArtA",
    description:
      "Kliederen, smossen, experimenteren met scheerschuim",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Zwemmen op stoeltjes met Marjolijn en Jess",
    description:
      "Niet zo goed opgelet tijdens de zwemles vroeger? Deze kleppers leren je zwemmen zoals zelfs dolfijnen dat niet kunnen!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Watercantus met Liza",
    description:
      "Heb je een stem als een nachtegaal en wil je die graag eens laten horen? IO VIVAT, IO VIVAT! ",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Anderstaligheid met Jeugdwerk voor Allen",
    description:
      "Hoe ga je om met anderstalige kinderen en jongeren in je Chirogroep? En hoe vinden zij de weg naar jullie groep?",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Pimp je spel met het Afdelingscafé ",
    description:
      "Klassieke spelletjes omtoveren tot originele en creatieve spelen",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Extreme spelen met Brecht",
    description:
      "Pijn = fijn! Deze spelletjes zijn zeker niet voor WATJES! Schrijf je in en leef je lekker uit!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Diablo met Thomas",
    description:
      "Indruk maken op meisjes/jongens? Thomas leert je de coolste trucjes met zijn diablo!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Djembe met Mondo Djembe ",
    description:
      "Leef je uit op Afrikaanse trommels en ga helemaal op in het ritme!",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Kleine probleempjes op de LK met Groepsleidingscommissie",
    description:
      "Tijdens deze workshop krijg je de beste tips en tricks om kleine probleempjes en ergernissen tijdens een leidingskring aan te pakken.",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  },
  {
    title: "Dansen met Johannes",
    description:
      "Gooi die dansbenen maar lekker los, leer hoe je dansjes tot een hoger niveau kan tillen en improviseer erop los",
    startTime: "16u30",
    endTime: "17u30",
    location: "Podium"
  }
];

export const standardMapIconSize = 40;

export const entreeIcon = {
  top: 543,
  left: 260,
  height: 40,
  width: 120,
  src: require("../images/icon_inkom.png")
}

export const mapIcons = [
  {
    top: 470,
    left: 170,
    size: standardMapIconSize,
    src: require("../images/icon_airmobiel.png")
  },
  {
    top: 350,
    left: 170,
    size: standardMapIconSize,
    src: require("../images/icon_bingo.png")
  },
  {
    top: 375,
    left: 285,
    size: standardMapIconSize,
    src: require("../images/icon_bonnen.png")
  },
  {
    top: 410,
    left: 20,
    size: standardMapIconSize,
    src: require("../images/icon_bumperbal.png")
  },
  {
    top: 400,
    left: 230,
    size: standardMapIconSize,
    src: require("../images/icon_chirocafe.png")
  },
  {
    top: 500,
    left: 280,
    size: standardMapIconSize,
    src: require("../images/icon_ehbo.png")
  },
  {
    top: 450,
    left: 230,
    size: standardMapIconSize,
    src: require("../images/icon_eten.png")
  },
  {
    top: 135,
    left: 95,
    size: standardMapIconSize,
    src: require("../images/icon_fluoparty.png")
  },
  {
    top: 135,
    left: 185,
    size: standardMapIconSize,
    src: require("../images/icon_grimeren.png")
  },
  {
    top: 500,
    left: 235,
    size: standardMapIconSize,
    src: require("../images/icon_klimmen.png")
  },
  {
    top: 245,
    left: 330,
    size: standardMapIconSize,
    src: require("../images/icon_lasershooten.png")
  },
  {
    top: 295,
    left: 285,
    size: standardMapIconSize,
    src: require("../images/icon_markt.png")
  },
  {
    top: 250,
    left: 10,
    size: 60,
    src: require("../images/icon_massaspel.png")
  },
  {
    top: 450,
    left: 75,
    size: standardMapIconSize,
    src: require("../images/icon_mollenspel.png")
  },
  {
    top: 360,
    left: 20,
    size: standardMapIconSize,
    src: require("../images/icon_photobooth.png")
  },
  {
    top: 325,
    left: 330,
    size: standardMapIconSize,
    src: require("../images/icon_podium.png")
  },
  {
    top: 270,
    left: 150,
    size: 75,
    src: require("../images/icon_radiotoren.png")
  },
  {
    top: 460,
    left: 122,
    size: standardMapIconSize,
    src: require("../images/icon_rodeo.png")
  },
  {
    top: 150,
    left: 240,
    size: standardMapIconSize,
    src: require("../images/icon_silentdisco.png")
  },
  {
    top: 245,
    left: 285,
    size: standardMapIconSize,
    src: require("../images/icon_singstarendance.png")
  },
  {
    top: 400,
    left: 80,
    size: standardMapIconSize,
    src: require("../images/icon_springkastelen.png")
  },
  {
    top: 375,
    left: 325,
    size: standardMapIconSize,
    src: require("../images/icon_toog.png")
  },
  {
    top: 130,
    left: 140,
    size: standardMapIconSize,
    src: require("../images/icon_vlaamsekermis.png")
  },
  {
    top: 170,
    left: 300,
    size: standardMapIconSize,
    src: require("../images/icon_wc.png")
  },
  {
    top: 215,
    left: 165,
    size: 50,
    src: require("../images/icon_workshops.png")
  }
];

export const podiumTable = [
  {
    title: "Slot show",
    startTime: "20h30",
    endTime: "22h00",
    location: "Podium",
    top: 410,
    height: 105
  },
  {
    title: "Fuif",
    startTime: "22h00",
    endTime: "03h00",
    location: "Podium",
    top: 5,
    height: 105
  }
];

export const middenPlein = [
  {
    title: "Radio Toren",
    startTime: "15h30",
    endTime: "16h30",
    location: "Schuim",
    top: 5,
    height: 70
  },
  {
    title: "Workshops",
    startTime: "16h30",
    endTime: "17h30",
    location: "Schuim",
    top: 5,
    height: 70
  },
  {
    title: "Radio Toren",
    startTime: "17h30",
    endTime: "19h00",
    location: "Schuim",
    top: 5,
    height: 105
  },
  {
    title: "Massa spel",
    startTime: "19h00",
    endTime: "20h00",
    location: "Schuim",
    top: 5,
    height: 70
  },
  {
    title: "Kienen",
    startTime: "20h00",
    endTime: "20h30",
    location: "Chiro Café",
    top: 5,
    height: 60
  }
];

export const chiroCafeTable = [
  {
    title: "Chillen @ Chiro café",
    startTime: "15h30",
    endTime: "21h00",
    location: "Chiro Café",
    top: 5,
    height: 430
  }
];

export const frietTable = [
  {
    title: "Finger Food",
    startTime: "18h00",
    endTime: "21h00",
    location: "Food Wood",
    top: 220,
    height: 215
  }
];

export const randAnimatieTable = [
  {
    title: "Rand animatie",
    startTime: "15h30",
    endTime: "20h30",
    location: "Animatie",
    top: 5,
    height: 390
  }
];

export const chiroKraampjesTable = [
  {
    title: "Chirokraampjes",
    startTime: "17h30",
    endTime: "19h00",
    location: "Markt",
    top: 185,
    height: 105
  }
];

export const tableTitles = [
  "Midden plein",
  "Podium",
  "Chiro Café",
  "Food",
  "Animatie",
  "Markt"
];
