/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import { createBottomTabNavigator } from "react-navigation";
import IonIcons from "react-native-vector-icons/Ionicons";

import SplashScreen from "./screens/SplashScreen";
import NewsScreen from "./screens/NewsScreen";
import TimetableScreen from "./screens/TimetableScreen";
import WorkshopsScreen from "./screens/WorkshopsScreen";
import MapScreen from "./screens/MapScreen";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

export default createBottomTabNavigator(
  {
    Home: NewsScreen,
    Workshops: WorkshopsScreen,
    Map: MapScreen,
    Timetable: TimetableScreen
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName == "Home") {
          iconName = "ios-paper";
        } else if (routeName == "Timetable") {
          iconName = "ios-clock";
        } else if (routeName == "Map") {
          iconName = "ios-map";
        } else if (routeName == "Workshops") {
          iconName = "ios-man";
        }
        return <IonIcons name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      showLabel: false,
      activeBackgroundColor: "#2D9CDB",
      inactiveBackgroundColor: "#2D9CDB",
      activeTintColor: "#FFF",
      inactiveTintColor: "#FFFFFF88",
      style: {
        backgroundColor: "#2D9CDB"
      }
    }
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
