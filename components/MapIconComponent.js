/**
 * @flow
 */

import React, { Component } from "react";
import { View, StyleSheet, Image } from "react-native";

class MapIconComponent extends Component {
  render() {
    let { top, left, size, src } = this.props.data;
    let randomTop = top == -1;
    let randomLeft = left == -1;
    return (
      <Image
        style={{
          position: "absolute",
          top: randomTop ? Math.floor(Math.random() * 540) + 40 : top,
          left: randomLeft ? Math.floor(Math.random() * 360) + 40 : left,
          height: size,
          width: size
        }}
        source={src}
      />
    );
  }
}

export default MapIconComponent;
