/**
 * @flow
 */

import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

class TransportContainer extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={styles.container}>
          <Icon
            name="ios-train"
            size={44}
            color="#FFF"
            style={{ alignSelf: "center" }}
          />
          <View style={styles.textContainer}>
            <Text style={styles.title}>Tjoeke Tjoeke Tuut Tuuut!</Text>
            <Text style={styles.description}>
              Hoe geraak je op Ambriage? Je ontdenkt het hier!
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#20c997DD",
    borderRadius: 5,
    flexDirection: "row",
    padding: 15,
    marginLeft: 20,
    marginRight: 20
  },
  description: {
    fontSize: 15,
    color: "#FFF"
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#FFF",
    marginBottom: 10
  },
  textContainer: {
    justifyContent: "space-between",
    marginLeft: 15
  }
});

export default TransportContainer;
