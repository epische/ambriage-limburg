/**
 * @flow
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  Linking
} from "react-native";

class InfoContainer extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>"Wij maken eigenlijk het internet"</Text>
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={require("../images/test.jpeg")} />
          <View style={styles.buttonTextContainer}>
            <Text style={styles.description}>
              Benieuwd wat er allemaal achter de schermen gebeurd?
            </Text>
            <TouchableOpacity onPress={this._openYoutubeBlog}>
              <Text style={styles.buttonText}>Naar de blog!</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  _openYoutubeBlog = () => {
    Linking.canOpenURL(this.props.url).then(supported => {
      if (supported) {
        Linking.openURL(this.props.url);
      } else {
        console.log("Don't know how to open URI: " + this.props.url);
      }
    });
  };
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    padding: 15,
    borderRadius: 5,
    backgroundColor: "#17a2b8DD",
    margin: 20
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#FFF",
    marginBottom: 10,
    alignSelf: "center"
  },
  imageContainer: {
    flexDirection: "row"
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 3,
    alignSelf: "center"
  },
  description: {
    fontSize: 15,
    color: "#FFF"
  },
  buttonText: {
    fontSize: 13,
    color: "#FFF",
    fontWeight: "bold",
    backgroundColor: "#28A745",
    alignSelf: "flex-start",
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 5,
    marginTop: 15
  },
  buttonTextContainer: {
    marginRight: 15,
    marginLeft: 15,
    flex: 1,
    justifyContent: "space-between"
  }
});

export default InfoContainer;
