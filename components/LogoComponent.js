/**
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet, Image } from "react-native";

class ComponentName extends Component {
  render() {
    return (
      <Image
        style={styles.logoLimburg}
        source={require("../images/logo_limburg.png")}
      />
    );
  }
}

const styles = StyleSheet.create({
  logoLimburg: {
    width: 318,
    height: 140,
    marginTop: 20,
    marginBottom: 20,
    alignSelf: "center"
  }
});

export default ComponentName;
