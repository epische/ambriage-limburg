/**
 * @flow
 */

import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

import { getColorFromType } from "../general/functions";

class TimetableComponent extends Component {
  render() {
    let { location, top, startTime, title, endTime, height } = this.props.event;
    console.log(this.props.event);
    return (
      <View
        style={[
          styles.container,
          {
            borderLeftColor: getColorFromType(location),
            marginTop: top,
            height: height
          }
        ]}
      >
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.time}>
          {startTime} - {endTime}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 14,
    fontWeight: "bold"
  },
  time: {
    fontSize: 12,
    fontWeight: "bold"
  },
  container: {
    borderRadius: 5,
    backgroundColor: "#FFFFFFDD",
    borderLeftColor: "#000",
    borderLeftWidth: 3,
    paddingLeft: 12,
    paddingTop: 12,
    marginTop: 10,
    width: 150,
    height: 70
  }
});

export default TimetableComponent;
