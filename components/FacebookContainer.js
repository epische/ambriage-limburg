/**
 * @flow
 */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Linking,
  TouchableOpacity
} from "react-native";

import Icon from "react-native-vector-icons/FontAwesome";

class FacebookContainer extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this._openFacebook}>
        <View style={styles.container}>
          <Icon
            name="facebook-square"
            size={44}
            color="#FFF"
            style={{ alignSelf: "center" }}
          />
          <View style={styles.textContainer}>
            <Text style={styles.title}>Blijf van alles op de hoogte</Text>
            <Text style={styles.description}>Bezoek onze facebook pagina</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  _openFacebook = () => {
    Linking.canOpenURL(this.props.url).then(supported => {
      if (supported) {
        Linking.openURL(this.props.url);
      } else {
        console.log("Don't know how to open URI: " + this.props.url);
      }
    });
  };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#007bffDD",
    borderRadius: 5,
    flexDirection: "row",
    padding: 15,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20
  },
  description: {
    fontSize: 15,
    color: "#FFF"
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#FFF",
    marginBottom: 10
  },
  textContainer: {
    justifyContent: "space-between",
    marginLeft: 15
  }
});

export default FacebookContainer;
