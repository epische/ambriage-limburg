/**
 * @flow
 */

import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

class LocationContainer extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>WORKSHOPS</Text>
        <Text style={styles.description}>
          Hier wat informatie over de vijver
        </Text>
        <View style={styles.separator} />
        <Text style={styles.subTitle}>Evenementen op de Vijver</Text>
        {this._renderWorkshops(workshops)}
      </View>
    );
  }

  _renderWorkshops = ws => {
    return ws.map((w, i) => this._renderWorkshop(w, i, ws.length));
  };

  _renderWorkshop = (workshop, index, size) => {
    return (
      <View style={{ flex: 1, flexDirection: "column" }} key={index}>
        <Text style={styles.workshopTitle}>{workshop.title}</Text>
        <Text style={styles.workshopDescription}>{workshop.description}</Text>
        <Text style={styles.workshopBoldTime}>
          {workshop.startTime} - {workshop.endTime}
        </Text>
        <Text style={styles.workshopBoldDescription}>{workshop.location}</Text>
        {index != size - 1 ? <View style={styles.separator} /> : <View />}
      </View>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: "#6610f2DD",
    borderRadius: 5,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20
  },
  title: {
    fontSize: 36,
    color: "#FFF",
    textAlign: "center",
    fontWeight: "bold",
    marginTop: 20,
    marginBottom: 24
  },
  description: {
    fontSize: 16,
    color: "#FFF",
    textAlign: "center"
  },
  subTitle: {
    fontSize: 24,
    color: "#FFF",
    fontWeight: "bold",
    marginBottom: 20
  },
  workshopTitle: {
    fontSize: 18,
    color: "#FFF",
    fontWeight: "bold"
  },
  workshopDescription: {
    fontSize: 16,
    color: "#FFF",
    marginTop: 16
  },
  workshopBoldDescription: {
    fontSize: 16,
    color: "#FFF",
    fontWeight: "bold"
  },
  workshopBoldTime: {
    fontSize: 16,
    color: "#FFF",
    fontWeight: "bold",
    marginTop: 10
  },
  separator: {
    borderBottomColor: "#FFF",
    borderBottomWidth: 1,
    marginTop: 20,
    marginBottom: 20
  }
});

const workshops = [
  {
    title: "Origami met Joris",
    description:
      "Steel de show op familiefeestjes met de meest epische origami constructies. Vliegtuigskes, bloemekes, de Mona Lisa in 3D, noem maar op, Joris fixt allesh.",
    startTime: "13u30",
    endTime: "16u30",
    location: "ergens"
  },
  {
    title: "Origami met Joris",
    description:
      "Steel de show op familiefeestjes met de meest epische origami constructies. Vliegtuigskes, bloemekes, de Mona Lisa in 3D, noem maar op, Joris fixt allesh.",
    startTime: "13u30",
    endTime: "16u30",
    location: "ergens"
  }
];

export default LocationContainer;
