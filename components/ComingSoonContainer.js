/**
 * @flow
 */

import React, { Component } from "react";
import { View, Text, Image, StyleSheet } from "react-native";

class ComingSoonContainer extends Component {
  render() {
    return (
      <View
        style={{
          backgroundColor: this.props.backgroundColor,
          borderRadius: 5,
          marginLeft: 20,
          marginRight: 20,
          marginBottom: 150
        }}
      >
        <Text style={styles.title}>{this.props.title}</Text>
        <Image
          style={styles.image}
          source={require("../images/coming_soon.png")}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 36,
    color: "#FFF",
    textAlign: "center",
    fontWeight: "bold",
    marginTop: 20,
    marginBottom: 24
  },
  image: {
    width: 280,
    height: 100,
    alignSelf: "center",
    marginBottom: 50,
    marginTop: 30
  }
});

export default ComingSoonContainer;
