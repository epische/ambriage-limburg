/**
 * @flow
 */

import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

class CountDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secondsRemaining: this.props.initialSecondsRemaining,
      timeoutId: null,
      previousSeconds: null,
      timeElapsed: false
    };

    this.mounted = false;

    this.tick = this.tick.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    this.tick();
  }

  componentWillReceiveProps(newProps) {
    if (this.state.timeoutId) {
      clearTimeout(this.state.timeoutId);
    }
    this.setState({
      previousSeconds: null,
      secondsRemaining: newProps.initialSecondsRemaining
    });
  }

  componentDidUpdate(nextProps, nextState) {
    if (
      !this.state.previousSeconds &&
      this.state.secondsRemaining > 0 &&
      this.mounted
    ) {
      this.tick();
    }
  }

  componentWillUnmount() {
    this.mounted = false;
    clearTimeout(this.state.timeoutId);
  }

  tick() {
    const currentSeconds = Date.now();
    const dt = this.state.previousSeconds
      ? currentSeconds - this.state.previousSeconds
      : 0;
    const interval = this.props.interval;

    // correct for small variations in actual timeout time
    const intervalSecondsRemaing = interval - (dt % interval);
    let timeout = intervalSecondsRemaing;

    if (intervalSecondsRemaing < interval / 2.0) {
      timeout += interval;
    }

    const secondsRemaining = Math.max(this.state.secondsRemaining - dt, 0);
    const isComplete = this.state.previousSeconds && secondsRemaining <= 0;

    if (this.mounted) {
      if (this.state.timeoutId) {
        clearTimeout(this.state.timeoutId);
      }
      this.setState({
        timeoutId: isComplete ? null : setTimeout(this.tick, timeout),
        previousSeconds: currentSeconds,
        secondsRemaining: secondsRemaining
      });
    }

    if (isComplete) {
      if (this.props.onTimeElapsed) {
        this.props.onTimeElapsed();
      }
      this.state.timeElapsed = true;
      return;
    }

    if (this.props.onTick) {
      this.props.onTick(secondsRemaining);
    }
  }

  getFormattedTime(milliseconds) {
    if (this.props.formatSecondsRemaining) {
      return this.props.formatSecondsRemaining(milliseconds);
    }

    const totalSeconds = Math.round(milliseconds / 1000);

    let seconds = parseInt(totalSeconds % 60, 10);
    let minutes = parseInt(totalSeconds / 60, 10) % 60;
    let hours = parseInt(totalSeconds / 3600, 10) % 24;
    let days = parseInt(totalSeconds / 86400, 10);

    seconds = seconds < 10 ? "0" + seconds : seconds;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    hours = hours < 10 ? "0" + hours : hours;
    days = days < 10 ? "0" + days : days;

    //hours = hours === "00" ? "" : hours + ":";

    //return hours + minutes + ':' + seconds;
    return {
      days,
      hours,
      minutes,
      seconds
    };
  }

  render() {
    let countDownTime = this.getFormattedTime(this.state.secondsRemaining);
    return this.state.timeElapsed ? (
      <View style={styles.fullContainer}>
        <Text style={styles.finished}>Geniet ervan!</Text>
      </View>
    ) : (
      this._drawTimer(countDownTime)
    );
  }

  _drawTimer = countDownTime => {
    return (
      <View style={styles.fullContainer}>
        <View style={styles.timeContainer}>
          <Text style={styles.text}>{countDownTime.days}</Text>
          <Text style={styles.subtitle}>DAGEN</Text>
        </View>
        <View style={styles.timeContainer}>
          <Text style={styles.text}>{countDownTime.hours}</Text>
          <Text style={styles.subtitle}>UUR</Text>
        </View>
        <View style={styles.timeContainer}>
          <Text style={styles.text}>{countDownTime.minutes}</Text>
          <Text style={styles.subtitle}>MIN.</Text>
        </View>
        <View style={styles.timeContainer}>
          <Text style={styles.text}>{countDownTime.seconds}</Text>
          <Text style={styles.subtitle}>SEC.</Text>
        </View>
      </View>
    );
  };
}

CountDown.defaultProps = {
  interval: 1000,
  formatSecondsRemaining: null,
  onTick: null,
  onTimeElapsed: null,
  allowFontScaling: false,
  style: {}
};

const styles = StyleSheet.create({
  timeContainer: {
    flexDirection: "column",
    paddingTop: 10,
    paddingBottom: 10,
    width: 70
  },
  fullContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    backgroundColor: "#DC3545DD",
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 5
  },
  text: {
    color: "#FFF",
    fontSize: 36,
    fontWeight: "bold",
    textAlign: "center"
  },
  subtitle: {
    color: "#FFF",
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center"
  },
  finished: {
    color: "#FFF",
    fontSize: 36,
    fontWeight: "bold",
    textAlign: "center",
    marginTop: 10,
    marginBottom: 10
  }
});

export default CountDown;
