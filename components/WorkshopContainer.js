/**
 * @flow
 */

import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

import Icon from "react-native-vector-icons/Ionicons";

import { workshops } from "../general/data";

class WorkshopContainer extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>WORKSHOPS</Text>
        {this._renderWorkshops(workshops)}
      </View>
    );
  }

  _renderWorkshops = ws => {
    return ws.map((w, i) => this._renderWorkshop(w, i, ws.length));
  };

  _renderWorkshop = (workshop, index, size) => {
    return (
      <View style={{ flex: 1, flexDirection: "column" }} key={index}>
        <Text style={styles.workshopTitle}>{workshop.title}</Text>
        <Text style={styles.workshopDescription}>{workshop.description}</Text>
        <Text style={styles.workshopBoldTime}>
          {workshop.startTime} - {workshop.endTime}
        </Text>
        <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
          <Text style={styles.workshopBoldDescription}>
            @{workshop.location}
          </Text>
        </View>
        {index != size - 1 ? <View style={styles.separator} /> : <View />}
      </View>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: "#20c997DD",
    borderRadius: 5,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20
  },
  title: {
    fontSize: 36,
    color: "#FFF",
    textAlign: "center",
    fontWeight: "bold",
    marginTop: 20,
    marginBottom: 24
  },
  workshopTitle: {
    fontSize: 18,
    color: "#FFF",
    fontWeight: "bold"
  },
  workshopDescription: {
    fontSize: 16,
    color: "#FFF",
    marginTop: 16
  },
  workshopBoldDescription: {
    fontSize: 16,
    color: "#FFF",
    fontWeight: "bold"
  },
  workshopBoldTime: {
    fontSize: 16,
    color: "#FFF",
    fontWeight: "bold",
    marginTop: 10
  },
  separator: {
    borderBottomColor: "#FFF",
    borderBottomWidth: 1,
    marginTop: 20,
    marginBottom: 20
  },
  showOnMapText: {
    fontSize: 14,
    color: "#FFF",
    marginLeft: 5,
    textDecorationLine: "underline"
  }
});

export default WorkshopContainer;
