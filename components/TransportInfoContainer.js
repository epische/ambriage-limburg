/**
 * @flow
 */

import React, { Component } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";

import Icon from "react-native-vector-icons/Ionicons";

class TransportInfoContainer extends Component {
  render() {
    let copy = {
      title: "WAAR IS DAT FEESTJE?",
      headerFirst: "Ambriage gaat door aan ",
      headerBold: "Hangaar 66 in Sint-Truiden. ",
      headerLast:
        "Je kan je aanmelden MET camping ticket van 15h tot 16h15 aan de camping, en vanaf 15h30 tot 16h30 aan de inkom op het terrein."
    };
    return (  
        <View style={styles.container}>
          <Text style={styles.title}>{copy.title}</Text>
          <View style={styles.descriptionContainer}>
            <Icon
              name="ios-map"
              size={44}
              color="#FFF"
              style={styles.mapIconStyle}
            />
            <View style={styles.locationContainer}>
              <Text style={styles.description}>
                {copy.headerFirst}
                <Text style={styles.boldDescription}>{copy.headerBold}</Text>
              </Text>
              <Text style={styles.description}>{copy.headerLast}</Text>
            </View>
          </View>

          <View style={styles.separator} />
          <View style={styles.descriptionContainer}>
            <Icon
              name="ios-car"
              size={44}
              color="#FFF"
              style={styles.iconStyle}
            />
            <View style={styles.locationContainer}>
              <Text style={styles.description}>
                Komt ge met den otto? Die kunt ge parkeren op de parking aan den
                Hangaar. Ge hebt geluk, want da kost gene frank om uw
                vierwieler daar een weekend te laten staan. Je geraakt er door
                het volgend adres in te geven in je gps (en de pijlen te volgen):
              </Text>
              <Text style={styles.boldDescription}>Lichtenberglaan</Text>
              <Text style={styles.boldDescription}>3800 Sint-Truiden</Text>
            </View>
          </View>
          <View style={styles.descriptionContainer}>
            <Icon
              name="ios-train"
              size={44}
              color="#FFF"
              style={styles.trainIconStyle}
            />
            <Text style={styles.description}>
              Een andere optie om op ambriage te geraken is met de
              trein of bus, het station van Sint-Truiden is 
              niet op wandel afstand. Maar er worden bussen voorzien 
              om van en naar het evenementen terrein te geraken.
              Zaterdag zijn bussen voorzien om 14h30, 15h en 15h30, 
              en zondag om 10h30, 11h en 11h30.
            </Text>
          </View>
          <View style={styles.descriptionContainer}>
            <Icon
              name="ios-bicycle"
              size={44}
              color="#FFF"
              style={styles.iconStyle}
            />
            <Text style={styles.description}>
              Er is een fietsenstalling aanwezig op het terrein, daar
              kan je veilig je stalen ros wegstallen.
            </Text>
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: "#20c997DD",
    borderRadius: 5,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20
  },
  title: {
    fontSize: 36,
    color: "#FFF",
    textAlign: "center",
    fontWeight: "bold",
    marginTop: 20
  },
  description: {
    fontSize: 16,
    color: "#FFF",
    flex: 1,
    flexWrap: "wrap"
  },
  boldDescription: {
    fontSize: 16,
    color: "#FFF",
    fontWeight: "bold",
    marginTop: 5
  },
  separator: {
    borderBottomColor: "#FFF",
    borderBottomWidth: 1,
    marginTop: 20
  },
  descriptionContainer: {
    flexDirection: "row",
    marginTop: 20
  },
  iconStyle: {
    marginRight: 10
  },
  trainIconStyle: {
    marginRight: 16,
    marginLeft: 8
  },
  mapIconStyle: {
    marginRight: 14,
    marginLeft: 2
  },
  locationContainer: {
    flexDirection: "column",
    flex: 1,
    marginRight: 10
  }
});

export default TransportInfoContainer;
