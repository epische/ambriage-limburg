/**
 * @flow
 */

import React, { Component } from "react";
import { View, Image, StyleSheet } from "react-native";

class SplashScreen extends Component {
  render() {
    return (
      <View>
        <Image
          style={styles.backgroundImage}
          source={require("../images/background_splash.png")}
        />
        <Image
          style={styles.logo}
          source={require("../images/logo_large.png")}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 50,
    width: 375,
    height: 726
  },
  logo: {
    position: "absolute",
    top: 430,
    left: 0,
    width: 375,
    height: 221
  }
});

export default SplashScreen;
