/**
 * @flow
 */

import React, { Component } from "react";
import { View, StyleSheet, ImageBackground, Image, Text } from "react-native";

import LocationContainer from "../components/LocationContainer";
import ComingSoonContainer from "../components/ComingSoonContainer";
import ScrollView, { ScrollViewChild } from "react-native-directed-scrollview";
import MapIconComponent from "../components/MapIconComponent";
import MapIconComponentNonSquare from "../components/MapIconComponentNonSquare";
import { mapIcons, entreeIcon } from "../general/data";

class MapScreen extends Component {
  render() {
    return (
      <View>
        <ImageBackground
          style={styles.backgroundImage}
          source={require("../images/background_splash.png")}
        >
          <ScrollView
            bounces={false}
            bouncesZoom={false}
            maximumZoomScale={3}
            minimumZoomScale={0.5}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.contentContainer}
            style={styles.container}
          >
            <ScrollViewChild scrollDirection={"both"}>
              <View
                style={{ flexDirection: "column", backgroundColor: "#FFF" }}
              >
                <Image
                  style={styles.map}
                  source={require("../images/plattegrond2019_blanco_small.png")}
                />
                {this._renderMapIcons()}
                <MapIconComponentNonSquare data={entreeIcon}/>
              </View>
            </ScrollViewChild>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }

  _renderMapIcons = () => {
    return mapIcons.map((w, i) => <MapIconComponent data={w} key={i} />);
  };
}

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 50,
    height: 900
  },
  logoLimburg: {
    width: 318,
    height: 125,
    marginTop: 20,
    marginBottom: 20,
    alignSelf: "center"
  },
  empty: {
    height: 150,
    width: 420
  },
  container: {
    flex: 1,
    backgroundColor: "#17a2b8DD"
  },
  contentContainer: {
    height: 800,
    width: 400,
    margin: 20
  },
  map: {
    height: 600,
    width: 406
  }
});

export default MapScreen;
