/**
 * @flow
 */

import React, { Component } from "react";
import {
  View,
  ImageBackground,
  StyleSheet,
  Image,
  ScrollView
} from "react-native";

import CountDown from "../components/CountDown";
import InfoContainer from "../components/InfoContainer";
import TransportContainer from "../components/TransportContainer";
import TransportInfoContainer from "../components/TransportInfoContainer";
import FacebookContainer from "../components/FacebookContainer";
import LogoComponent from "../components/LogoComponent";
import MassaspelContainer from "../components/MassaspelContainer";

class NewsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullTransport: false
    };
  }
  render() {
    let dDay = new Date(2018, 8, 1, 14, 0, 0, 0);
    let timeDiff = dDay - Date.now();
    let check = timeDiff <= 0;
    return (
      <View>
        <ImageBackground
          style={styles.backgroundImage}
          source={require("../images/background_splash.png")}
        >
          <ScrollView>
            <LogoComponent />
            <TransportInfoContainer/>
            <FacebookContainer url="https://www.facebook.com/events/554763918344009/" />
            <View style={styles.empty} />
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }

  _renderTransportContainer = () => {
    return !this.state.fullTransport ? (
      <TransportContainer onPress={() => this._setTransportVisibility(true)} />
    ) : (
      <TransportInfoContainer
        onPress={() => this._setTransportVisibility(false)}
      />
    );
  };

  _setTransportVisibility = visible => {
    this.setState({ fullTransport: visible });
  };
}

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 50
  },
  empty: {
    height: 150
  }
});

export default NewsScreen;
