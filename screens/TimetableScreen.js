/**
 * @flow
 */

import React, { Component } from "react";
import { View, StyleSheet, ImageBackground, Image, Text } from "react-native";

import ScrollView, { ScrollViewChild } from "react-native-directed-scrollview";

import TimetableComponent from "../components/TimetableComponent";
import {
  podiumTable,
  middenPlein,
  chiroCafeTable,
  frietTable,
  randAnimatieTable,
  chiroKraampjesTable,
  tableTitles
} from "../general/data";

import { getColorFromType } from "../general/functions";

class TimetableScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          style={styles.backgroundImage}
          source={require("../images/background_splash.png")}
        >
          {this._renderTimeTable()}
        </ImageBackground>
      </View>
    );
  }

  _renderTimeTable = () => {
    return (
      <ScrollView
        bounces={true}
        bouncesZoom={true}
        maximumZoomScale={1.05}
        minimumZoomScale={0.95}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.contentContainer}
        style={styles.container}
        ref={scroller => {
          this.scroller = scroller;
        }}
      >
        <ScrollViewChild scrollDirection={"both"}>
          <View style={{ flexDirection: "row", marginTop: 50 }}>
            <View style={{ flexDirection: "column" }}>
              {this._renderTable(middenPlein)}
            </View>
            <View style={{ marginLeft: 10, flexDirection: "column" }}>
              {this._renderTable(podiumTable)}
            </View>
            <View style={{ marginLeft: 10, flexDirection: "column" }}>
              {this._renderTable(chiroCafeTable)}
            </View>
            <View style={{ marginLeft: 10, flexDirection: "column" }}>
              {this._renderTable(frietTable)}
            </View>
            <View style={{ marginLeft: 10, flexDirection: "column" }}>
              {this._renderTable(randAnimatieTable)}
            </View>
            <View style={{ marginLeft: 10, flexDirection: "column" }}>
              {this._renderTable(chiroKraampjesTable)}
            </View>
          </View>
        </ScrollViewChild>
        <ScrollViewChild
          scrollDirection={"horizontal"}
          style={styles.horizontalContainer}
        >
          <View
            style={{
              borderRadius: 5,
              backgroundColor: "#FFFFFFAA",
              flexDirection: "row"
            }}
          >
            {this._renderTableTitles()}
          </View>
        </ScrollViewChild>
      </ScrollView>
    );
  };

  _renderTable = table => {
    return table.map((w, i) => <TimetableComponent event={w} key={i} />);
  };

  _renderTableTitles = () => {
    return tableTitles.map((t, i) => (
      <View style={{ flexDirection: "row", alignItems: "center" }} key={i}>
        <View
          style={[
            styles.locationColorContainer,
            { backgroundColor: getColorFromType(t) }
          ]}
        />
        <Text style={styles.locationTitle}>{t}</Text>
      </View>
    ));
  };
}

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 50,
    flex: 1
  },
  container: {
    flex: 1,
    backgroundColor: "#17a2b8DD"
  },
  contentContainer: {
    height: 700,
    width: 1650,
    margin: 20
  },
  horizontalContainer: {
    position: "absolute",
    left: 0,
    top: 0
  },
  locationTitle: {
    width: 120,
    fontSize: 18,
    fontWeight: "bold",
    color: "#000",
    marginRight: 10,
    marginBottom: 10,
    marginTop: 10
  },
  locationColorContainer: {
    width: 15,
    height: 15,
    backgroundColor: "#F00",
    marginLeft: 10,
    marginRight: 5,
    borderRadius: 5
  },
  empty: {
    height: 150
  }
});

export default TimetableScreen;
