/**
 * @flow
 */

import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView
} from "react-native";

import WorkshopsContainer from "../components/WorkshopContainer";
import LogoComponent from "../components/LogoComponent";

class WorkshopsScreen extends Component {
  render() {
    return (
      <View>
        <ImageBackground
          style={styles.backgroundImage}
          source={require("../images/background_splash.png")}
        >
          <ScrollView>
            <LogoComponent />
            <WorkshopsContainer />
            <View style={styles.empty} />
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 50
  },
  empty: {
    height: 150
  }
});

export default WorkshopsScreen;
